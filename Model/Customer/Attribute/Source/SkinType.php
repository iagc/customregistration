<?php


namespace IAGC\CustomRegistration\Model\Customer\Attribute\Source;

class SkinType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => '1', 'label' => __('Normal')],
                ['value' => '2', 'label' => __('Dry')],
                ['value' => '3', 'label' => __('Oily')],
                ['value' => '4', 'label' => __('Combination')]
            ];
        }
        return $this->_options;
    }
}
